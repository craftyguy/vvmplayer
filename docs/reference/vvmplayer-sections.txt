<SECTION>
<FILE>vvmplayer-utils</FILE>
vvmplayer_utils_get_main_thread
VVMPLAYER_IS_MAIN_THREAD
</SECTION>

<SECTION>
<FILE>vvmplayer-application</FILE>
<TITLE>VvmApplication</TITLE>
VvmApplication
vvmplayer_application_new
</SECTION>

<SECTION>
<FILE>vvmplayer-settings</FILE>
<TITLE>VvmSettings</TITLE>
VvmSettings
vvmplayer_settings_new
vvmplayer_settings_get_is_first_run
vvmplayer_settings_get_window_maximized
vvmplayer_settings_set_window_maximized
vvmplayer_settings_get_window_geometry
vvmplayer_settings_set_window_geometry
</SECTION>

